// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, set, ref, update } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, onAuthStateChanged, signOut } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCSnrw0PgT8_GMDhIbjGk7flHFrsd4czyc",
    authDomain: "pagweb-fd59e.firebaseapp.com",
    databaseURL:"https://pagweb-fd59e-default-rtdb.firebaseio.com",
    projectId: "pagweb-fd59e",
    storageBucket: "pagweb-fd59e.appspot.com",
    messagingSenderId: "566300436662",
    appId: "1:566300436662:web:524e9f3ed3e6b794889e17"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const auth = getAuth();

//declaracion de objetos
var btnIniciar = document.getElementById('btnIniciar');
var btnLimpiar = document.getElementById('btnLimpiar');

var password = "";
var email = "";


//crear usuario de inicio
function iniciarSesion(){
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var username = document.getElementById('username').value;

    createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
    alert('Usuario creado');
    // ...
    })
    .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;

    alert('Algo salio mal');
    // ..
    });
}

//btnIniciar.addEventListener('click', iniciarSesion);

//Iniciar secion
function iniciarUsuario(){
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;

    signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in 
      const user = userCredential.user;

      const dt = new Date();
       update(ref(db, 'users/' + user.uid),{
        last_login: dt,
      })

       alert('Usuario logeado!');
       window.location.href = "/html/administrador.html";
      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;

      alert('Email o contraseña incorrectos');
});
}

function escribirInputs(){
  document.getElementById('email').value = email;
  document.getElementById('password').value = password;
}

function limpiar(){
  email = "";
  password = "";
  escribirInputs();
}

btnIniciar.addEventListener('click', iniciarUsuario);
btnLimpiar.addEventListener('click', limpiar);