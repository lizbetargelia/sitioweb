// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import{ getDatabase,onValue, ref, set, child, get, update, remove }
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import{getStorage,ref as refS, uploadBytes, getDownloadURL} 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCSnrw0PgT8_GMDhIbjGk7flHFrsd4czyc",
  authDomain: "pagweb-fd59e.firebaseapp.com",
  databaseURL:"https://pagweb-fd59e-default-rtdb.firebaseio.com",
  projectId: "pagweb-fd59e",
  storageBucket: "pagweb-fd59e.appspot.com",
  messagingSenderId: "566300436662",
  appId: "1:566300436662:web:524e9f3ed3e6b794889e17"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

//declaracion de objetos
var btnInsertar = document.getElementById('btnInsertar');
var btnBuscar = document.getElementById('btnBuscar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');
var btnTodos = document.getElementById('btnTodos');
var btnLimpiar = document.getElementById('btnLimpiar');
var archivo = document.getElementById('archivo');
var btnVerImagen = document.getElementById('btnVerImagen');
var lista = document.getElementById('lista');
var productosAlta = document.getElementById('productosAlta')

var codigo = "";
var nombre = "";
var descrip = "";
var precio = "";
var imgNombre = "";
var url = "";
var status =0;

function leerInputs(){
    codigo = document.getElementById('codigo').value;
    nombre = document.getElementById('nombre').value;
    descrip = document.getElementById('descrip').value;
    precio = document.getElementById('precio').value;
    imgNombre = document.getElementById('imgNombre').value;
    url = document.getElementById('url').value;
    status = document.getElementById('status').value;

  
}

function mostrarDatos(){
  leerInputs();
  const dbref = ref(db);
  get(child(dbref,'productos/' + codigo)).then((snapshot)=>{
      if(snapshot.exists()){
          nombre = snapshot.val().nombre;
          descrip = snapshot.val().descrip;
          precio = snapshot.val().precio;
          imgNombre = snapshot.val().imgNombre;
          url = snapshot.val().url,
          status = snapshot.val().status;
          escribirInputs();
      }
      else{
          alert("No existe ese código");
      }
  }).catch((error)=>{
      alert("Surgió un error "+ error);
  });
}

function escribirInputs(){
    document.getElementById('codigo').value = codigo;
    document.getElementById('nombre').value = nombre;
    document.getElementById('descrip').value = descrip;
    document.getElementById('precio').value = precio;
    document.getElementById('imgNombre').value = imgNombre;
    document.getElementById('url').value = url;
    document.getElementById('status').value = status;
}

if(window.location.href == "https://programacionwebliz.000webhostapp.com/html/productos.html"){
  window.onload = mostrarProductos();
}
// mostrar datos
function mostrarProductos(){
const db = getDatabase();
const dbRef = ref(db, 'productos');
onValue(dbRef, (snapshot) => {
  if(lista){
    lista.innerHTML = "";
  }

snapshot.forEach((childSnapshot) => {
const childKey = childSnapshot.key;
const childData = childSnapshot.val();
if(lista){
  if(childData.status == 0){
    lista.innerHTML = "<div class='campo'> " + lista.innerHTML + childKey  +
    " | "+childData.nombre  + " | "+ childData.descrip +" | "+childData.precio + " | "+ childData.imgNombre +" | "+childData.url+" | "+childData.status+"<br> </div>";
  }
  if(childData.status == 1){
    lista.innerHTML = "<div class='campo'> " + lista.innerHTML + childKey  +
    " | "+childData.nombre  + " | "+ childData.descrip +" | "+childData.precio + " | "+ childData.imgNombre +" | "+childData.url+" | "+childData.status+"<br> </div>";
    }
}
  if(productosAlta){
    if(childData.status==0)
    {
        productosAlta.innerHTML =  productosAlta.innerHTML + " <div > " + "<h3>" +childData.nombre + "</h3>"+" <br> <img class='imgProd' src=' " + childData.url + "'> <br> <p class='descrip2'> " + "</p><p class='descrip2'> Descripción: "  + childData.descrip + "</p> <p class='descrip2'> Precio: $" + childData.precio+ " </p>" + "<hr>";
    }
    
    
  }
//console.log(childKey + ":");
//console.log(childData.nombre)
// ...
});
}, {
onlyOnce: true
});
}






function InsertarDatos(){
  leerInputs();
  set(ref(db, 'productos/' + codigo),{
      nombre:nombre,
      descrip:descrip,
      precio:precio,
      imgNombre:imgNombre,
      url:url,
      status:status
  }).then((response)=>{
      
      alert("Se agregó un registo con exito");
  mostrarProductos();
  }).catch((error)=>{
      
      alert("Surgió un error "+ error);
  })
}

function actualizar(){
leerInputs();
update(ref(db,'productos/'+ codigo),{
nombre:nombre,
descrip:descrip,
precio:precio,
imgNombre:imgNombre,
url:url,
status:status
}).then(()=>{
alert("Se realizó actualización");
mostrarProductos();
})
.catch(()=>{
alert("Causó Error " + error );
});
}

    //function borrar(){
    //leerInputs();
    //remove(ref(db,'productos/'+ codigo)).then(()=>{
    //alert("Se borro");
    //mostrarProductos();
    //})
    //.catch(()=>{
    //alert("Causo Error" + error );
    //});
    //}

function borrar(){
  leerInputs();
 update(ref(db,'productos/'+ codigo),{
  nombre:nombre,
  descrip:descrip,
  precio:precio,
  url:url,
  imgNombre:imgNombre,
  status:"1"
 }).then(()=>{
  alert("Se dio de baja exitosamente");
  limpiar();
  mostrarProductos();
 })
 .catch(()=>{
  alert("Causo Error " + error );
});
}

function limpiar(){
lista.innerHTML="";
codigo="";
nombre="";
descrip="";
precio="";
imgNombre="";
url="";
status="";
escribirInputs();
}

function cargarImagen(){
  const file = event.target.files[0];
  const name = event.target.files[0].name;

  const storage = getStorage();
  const storageRef = refS(storage, 'imagenes/' + name);

  //'file' comes from the blob or file API
  uploadBytes(storageRef,file).then((snapshot)=>{
    document.getElementById('imgNombre').value = name;
    alert('Se cargo el archivo');
  });
}

function descargarImagen(){
  archivo = document.getElementById('imgNombre').value;
  // Create a reference to the file we want to download
const storage = getStorage();
const storageRef = refS(storage, 'imagenes/' + archivo);

// Get the download URL
getDownloadURL(storageRef)
  .then((url) => {
    document.getElementById('url').value = url;
    document.getElementById('imagen').src=url;
    // Insert url into an <img> tag to "download"
  })
  .catch((error) => {
    // A full list of error codes is available at
    // https://firebase.google.com/docs/storage/web/handle-errors
    switch (error.code) {
      case 'storage/object-not-found':
        console.log("No existe el archivo")
        // File doesn't exist
        break;
      case 'storage/unauthorized':
        console.log("No tiene permisos")
        // User doesn't have permission to access the object
        break;
      case 'storage/canceled':
        console.log("Se cancelo o no tiene internet")
        // User canceled the upload
        break;

      // ...

      case 'storage/unknown':
        console.log("No sabemos que paso")
        // Unknown error occurred, inspect the server response
        break;
    }
  });
}

//eventos click
btnInsertar.addEventListener('click', InsertarDatos);
btnBuscar.addEventListener('click',mostrarDatos);
btnActualizar.addEventListener('click',actualizar);
btnTodos.addEventListener('click', mostrarProductos);
btnBorrar.addEventListener('click',borrar);
btnLimpiar.addEventListener('click', limpiar);
archivo.addEventListener('change', cargarImagen);
btnVerImagen.addEventListener('click', descargarImagen);